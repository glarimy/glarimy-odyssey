from dataclasses import dataclass
from abc import ABC, abstractmethod
import datetime
import re

@dataclass(frozen=True)
class Name:
    value: str
    def __post_init__(self):
        if self.value is None or len(self.value.strip()) < 8 or len(self.value.strip()) > 32:
            raise ValueError("Invalid Name")

@dataclass(frozen=True)
class PhoneNumber:
    value: int
    def __post_init__(self):
        if self.value < 9000000000:
            raise ValueError("Invalid Phone Number")

@dataclass
class User:
    _name: Name
    _phone: PhoneNumber
    _since: datetime.datetime

    def __post_init__(self):
        if self._name is None or self._phone is None:
            raise ValueError("Invalid user")
        if self._since is None: 
            self.since = datetime.datetime.now()

    @property
    def name(self) -> Name:
        return self._name

    @property
    def phone(self) -> PhoneNumber:
        return self._phone

    @phone.setter
    def phone(self, phone: PhoneNumber) -> None:
        if phone is None:
            raise ValueError("Invalid phone")
        self._phone = phone

    @property
    def since(self) -> datetime.datetime:
        return self._since

    def __str__(self):
        return self.name.value + " [" + str(self.phone.value) + "] since " + str(self.since)

class UserNotFoundException(Exception):
    pass

class UserRepository(ABC):
    @abstractmethod
    def fetch(self, name:Name) -> User:
        pass