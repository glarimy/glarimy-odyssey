from domain import Name, User, PhoneNumber, UserNotFoundException
from data import UserRepositoryImpl
from flask import Flask, jsonify, abort
from waitress import serve
import sys

class UserRecord:
    def toJSON(self):
        return {
            "name": self.name,
            "phone": self.phone,
            "since": self.since
        }

class UserController:
    def __new__(self):
        if not hasattr(self, 'instance'):
            self.instance = super().__new__(self)
        return self.instance

    def __init__(self):
        self._repo = UserRepositoryImpl()

    def find(self, name:str):
        try: 
            user: User = self._repo.fetch(Name(name))
            record: UserRecord = UserRecord()
            record.name = user.name.value
            record.phone = user.phone.value
            record.since = user.since
            return record
        except UserNotFoundException as e:
            return None

app = Flask(__name__)
print(app.config)

@app.route('/user/<name>')   
def get(name):
    print("Name " + str(name), file=sys.stderr)
    controller = UserController()
    record = controller.find(name)
    if record is None: 
        abort(404)
    else:
        resp = jsonify(record.toJSON())
        resp.status_code = 200
        return resp

serve(app, host="0.0.0.0", port=8080)