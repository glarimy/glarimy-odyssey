package com.glarimy.ums.domain;

import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity(name = "ums_users")
public class User {
	@EmbeddedId
	@Valid
	@NotNull
	private Name name;

	@Embedded
	@Valid
	@NotNull
	private PhoneNumber phone;
	
	private Date since;

	User() {

	}

	public User(Name name, PhoneNumber phone) {
		this.name = name;
		this.phone = phone;
		this.since = new Date();
	}

	public User(Name name, PhoneNumber phone, Date since) {
		this.name = name;
		this.phone = phone;
		this.since = since;
	}

	public Name getName() {
		return name;
	}

	public PhoneNumber getPhone() {
		return phone;
	}

	public void setPhone(PhoneNumber phone) {
		this.phone = phone;
	}

	public Date getSince() {
		return since;
	}
}
