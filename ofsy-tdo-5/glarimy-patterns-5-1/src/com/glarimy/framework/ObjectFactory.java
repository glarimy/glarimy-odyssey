package com.glarimy.framework;

import com.glarimy.ums.app.UserController;
import com.glarimy.ums.data.UserRepositoryImpl;
import com.glarimy.ums.domain.UserRepository;

public class ObjectFactory implements Factory {

	@Override
	public Object get(String key) {
		if (key == "app") {
			UserRepository repo = new UserRepositoryImpl();
			UserController controller = new UserController(repo);
			return controller;
		}

		throw new RuntimeException();
	}

}
