package com.glarimy.ums.domain;

public class PhoneNumber {
	private long value;

	public PhoneNumber(long value) {
		this.value = value;
	}
	
	public long getValue() {
		return this.value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (value ^ (value >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhoneNumber other = (PhoneNumber) obj;
		if (value != other.value)
			return false;
		return true;
	}

}
