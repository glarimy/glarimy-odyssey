package com.glarimy.ums.domain;

public interface UserRepository {
	public User save(User user);
}
