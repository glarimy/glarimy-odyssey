package com.glarimy.ums.domain;

import java.util.Date;

public class User {
	private Name name;
	private PhoneNumber phone;
	private Date since;

	public User(Name name, PhoneNumber phone) {
		this.name = name;
		this.phone = phone;
		this.since = new Date();
	}

	public User(Name name, PhoneNumber phone, Date since) {
		this.name = name;
		this.phone = phone;
		this.since = since;
	}

	public Name getName() {
		return name;
	}

	public PhoneNumber getPhone() {
		return phone;
	}

	public void setPhone(PhoneNumber phone) {
		this.phone = phone;
	}

	public Date getSince() {
		return since;
	}
}
