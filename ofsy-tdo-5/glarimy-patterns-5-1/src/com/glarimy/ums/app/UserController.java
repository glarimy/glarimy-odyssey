package com.glarimy.ums.app;

import com.glarimy.broker.Event;
import com.glarimy.broker.Publisher;
import com.glarimy.ums.domain.Name;
import com.glarimy.ums.domain.PhoneNumber;
import com.glarimy.ums.domain.User;
import com.glarimy.ums.domain.UserRepository;

public class UserController {
	private UserRepository repo;

	public UserController(UserRepository repo) {
		this.repo = repo;
	}

	public UserRecord add(NewUser newUser) {
		Name name = new Name(newUser.name);
		PhoneNumber phoneNumber = new PhoneNumber(newUser.phone);
		User user = new User(name, phoneNumber);
		user = repo.save(user);
		Event event = new Event("", "");
		Publisher publisher = new Publisher();
		publisher.publish(event);
		UserRecord record = new UserRecord(user.getName().getValue(), user.getPhone().getValue(), user.getSince());
		return record;
	}

}