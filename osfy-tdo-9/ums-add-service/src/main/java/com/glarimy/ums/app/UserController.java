package com.glarimy.ums.app;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.glarimy.ums.domain.Name;
import com.glarimy.ums.domain.PhoneNumber;
import com.glarimy.ums.domain.User;
import com.glarimy.ums.domain.UserRepository;

@RestController
public class UserController {
	Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserRepository repo;
	@Autowired
	private Validator validator;
	@Autowired
	private KafkaTemplate<String, UserRecord> template;

	@PostMapping("/user")
	public ResponseEntity<UserRecord> add(@RequestBody NewUser newUser) {
		logger.debug("Received new user");
		Name name = new Name(newUser.name);
		PhoneNumber phoneNumber = new PhoneNumber(newUser.phone);
		logger.debug("Building User domain object");
		User user = new User(name, phoneNumber);
		logger.debug("Validating User domain object");
		Set<ConstraintViolation<User>> violations = validator.validate(user);
		ResponseEntity<UserRecord> response;
		if (!violations.isEmpty()) {
			violations.forEach(v -> logger.debug(v.getMessage()));
			response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			logger.debug("Response: " + response);
		} else {
			logger.debug("Saving the User domain object");
			repo.save(user);
			logger.debug("Building the response");
			UserRecord record = new UserRecord(user.getName().getValue(), user.getPhone().getValue(), user.getSince());
			template.send("com.glarimy.ums.user.add", record);
			response = new ResponseEntity<>(record, HttpStatus.OK);
			logger.debug("Response: " + response);
		}
		logger.debug("returning the response");
		return response;
	}

}