package com.glarimy.ums.app;

public class NewUser {
	public String name;
	public long phone;

	public NewUser(String name, long phone) {
		this.name = name;
		this.phone = phone;
	}

}
