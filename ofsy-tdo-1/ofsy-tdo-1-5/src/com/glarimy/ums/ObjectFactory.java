package com.glarimy.ums;

public class ObjectFactory {
	public static Object get(String key) throws Exception {
		if (key == "user.repository")
			return InMemoryUserRepository.getInstance();
		else
			throw new Exception();
	}
}