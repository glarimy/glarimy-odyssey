package com.client;

import com.glarimy.framework.ObjectFactory;
import com.glarimy.ums.UserRepository;

public class Application {
	public static void main(String[] args) throws Exception {
		UserRepository repo = (UserRepository) ObjectFactory.get("user.repository");
		repo.add(9731423166L, "Krishna");
		String name = repo.find(9731423166L);
		System.out.println("Found " + name);
	}
}
