package com.glarimy.ums;

import java.util.HashMap;
import java.util.Map;

public class InMemoryUserRepository implements UserRepository {
	private Map<Long, String> entries;

	public InMemoryUserRepository() {
		entries = new HashMap<Long, String>();
	}

	@Override
	public void add(Long phone, String name) {
		entries.put(phone, name);
	}

	@Override
	public String find(Long phone) {
		return entries.get(phone);
	}
}