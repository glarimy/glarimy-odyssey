package com.client;

import com.glarimy.ums.InMemoryUserRepository;
import com.glarimy.ums.UserRepository;

public class Application {
	public static void main(String[] args) {
		UserRepository repo = new InMemoryUserRepository();
		repo.add(9731423166L, "Krishna");
		String name = repo.find(9731423166L);
		System.out.println("Found " + name);
	}
}
