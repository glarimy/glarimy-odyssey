package com.client;

import com.glarimy.ums.UserRepository;
import com.glarimy.ums.UserRepositoryFactory;

public class Application {
	public static void main(String[] args) {
		UserRepository repo = UserRepositoryFactory.get();
		repo.add(9731423166L, "Krishna");
		String name = repo.find(9731423166L);
		System.out.println("Found " + name);
	}
}
