package com.glarimy.ums;

public class UserRepositoryFactory {
	public static UserRepository get() {
		return InMemoryUserRepository.getInstance();
	}
}
