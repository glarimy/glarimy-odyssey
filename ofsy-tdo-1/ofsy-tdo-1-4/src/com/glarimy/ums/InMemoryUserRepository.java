package com.glarimy.ums;

import java.util.HashMap;
import java.util.Map;

public class InMemoryUserRepository implements UserRepository {
	private static InMemoryUserRepository INSTANCE = null;
	private Map<Long, String> entries;

	private InMemoryUserRepository() {
		entries = new HashMap<Long, String>();
	}

	public static InMemoryUserRepository getInstance() {
		if (INSTANCE == null)
			INSTANCE = new InMemoryUserRepository();
		return INSTANCE;
	}

	@Override
	public void add(Long phone, String name) {
		entries.put(phone, name);
	}

	@Override
	public String find(Long phone) {
		return entries.get(phone);
	}
}