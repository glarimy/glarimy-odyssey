package com.glarimy.ums;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserAddService {

	public static void main(String[] args) {
		SpringApplication.run(UserAddService.class, args);
	}

}
