package com.glarimy.ums.app;

import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UserSerializer implements Serializer<UserRecord> {

	@Override
	public byte[] serialize(String topic, UserRecord user) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsBytes(user);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}

}
