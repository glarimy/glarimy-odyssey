import repo from '../data/UserRepository.js'
import Users from './Users.js'
import UserRecord from './UserRecord.js';

class UserController{
    async searchByPhoneNumber(phoneNumber){
        let entities = await repo.findByPhoneNumber(phoneNumber);
        let dtos = [];
        for(let entity of entities){
            dtos.push(new UserRecord(entity).toJSON())
        }
        return new Users(dtos);
    }
}

const controller = new UserController();
export default controller;