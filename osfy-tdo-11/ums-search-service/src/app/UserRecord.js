class UserRecord{
    constructor(user){
        this.name = user.getName().getValue();
        this.phoneNumber = user.getPhoneNumber().getValue();
        this.since = user.getSince();
    }
    toJSON(){
        return {
            name: this.name,
            phoneNumber: this.phoneNumber,
            since: this.since
        }
    }
}

export default UserRecord;