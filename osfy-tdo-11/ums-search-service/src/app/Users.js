class Users{
    constructor(records){
        this.records = records;
        this.size = records.length;
    }
    toJSON(){
        return {
            size: this.size,
            users: this.records
        }
    }
}

export default Users;