class Name{
    #value;
    constructor(value){
        if (value == null || value.trim().length < 8 || value.trim().length > 32)
            throw "Invalid Name";
        this.#value = value;
    }

    getValue(){
        return this.#value;
    }

    equals(other){
        if(this.constructor.name === other.constructor.name && this.#value === other.getValue())
            return true;
        return false;
    }
}

export default Name;