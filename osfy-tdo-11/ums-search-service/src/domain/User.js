class User{
    #name;
    #phoneNumber;
    #since;

    constructor(name, phoneNumber, since){
        if(name == null || phoneNumber == null || since ==null)
            throw "Invalid user"
        this.#name = name;
        this.#phoneNumber = phoneNumber;
        this.#since = since;
    }

    getName(){
        return this.#name;
    }

    getPhoneNumber(){
        return this.#phoneNumber;
    }

    setPhone(phoneNumber){
        if(phoneNumber == null)
            throw "Invalid phone number";
        this.#phoneNumber = phoneNumber;
    }

    getSince() {
        return this.#since;
    }

    toString() {
        return `${this.#name} ${this.#phoneNumber} ${this.#since}`
    }
}

export default User;