package com.glarimy.framework;

import java.io.FileReader;
import java.util.Properties;

public class ObjectFactory implements Factory {
	private Properties props;

	public ObjectFactory(String source) throws Exception {
		props = new Properties();
		props.load(new FileReader(source));
	}

	@Override
	public Object get(String key) throws Exception {
		String className = props.getProperty(key);
		Class<?> claz = Class.forName(className);
		Object o = null;

		try {
			o = claz.newInstance();
		} catch (IllegalAccessException | InstantiationException e) {
			Singleton singleton = claz.getAnnotation(Singleton.class);
			if (singleton != null) {
				String getter = singleton.getter();
				o = claz.getMethod(getter).invoke(claz);
			} else {
				o = claz.getMethod("getInstance").invoke(claz);
			}
		}

		String proxyClassName = props.getProperty(key + ".proxy");
		if (proxyClassName == null)
			return o;
		Class<?> proxyClass = Class.forName(proxyClassName);
		return proxyClass.getConstructor(claz.getInterfaces()[0]).newInstance(o);
	}
}