package com.glarimy.ums;

import com.glarimy.lib.FileJournal;

public class UserRepositoryJournalProxy implements UserRepository {
	private UserRepository target;
	private FileJournal journal;

	public UserRepositoryJournalProxy(UserRepository target) throws Exception {
		this.target = target;
		this.journal = new FileJournal();
	}

	@Override
	public void add(Long phone, String name) {
		journal.save("c=repository, s=add, a=" + phone + "&" + name);
		target.add(phone, name);
		journal.save("c=repository, s=add, r=void");
	}

	@Override
	public String find(Long phone) {
		journal.save("c=repository, s=find, a=:" + phone);
		String name = target.find(phone);
		journal.save("c=repository, s=find, r=" + name);
		return name;
	}
}