package com.glarimy.ums;

public class UserRepositoryJournalProxy implements UserRepository {
	private UserRepository target;

	public UserRepositoryJournalProxy(UserRepository target) {
		this.target = target;
	}

	@Override
	public void add(Long phone, String name) {
		System.out.println("c=repository, s=add, a=" + phone + "&" + name);
		target.add(phone, name);
		System.out.println("c=repository, s=add, r=void");
	}

	@Override
	public String find(Long phone) {
		System.out.println("c=repository, s=find, a=" + phone);
		String name = target.find(phone);
		System.out.println("c=repository, s=find, r=" + name);
		return name;
	}
}