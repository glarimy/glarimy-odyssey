package com.glarimy.ums;

import com.glarimy.framework.Factory;
import com.glarimy.framework.ObjectFactory;

public class UserRepositoryJournalProxy implements UserRepository {
	private UserRepository target;
	private JournalAdapter adapter;

	public UserRepositoryJournalProxy(UserRepository target) throws Exception {
		this.target = target;
		Factory factory = new ObjectFactory("config.properties");
		this.adapter = (JournalAdapter) factory.get("adapter");
	}

	@Override
	public void add(Long phone, String name) {
		adapter.record("c=repository, s=add, a=" + "&" + name);
		target.add(phone, name);
		adapter.record("c=repository, s=add, r=void");
	}

	@Override
	public String find(Long phone) {
		adapter.record("c=repository, s=find, a=" + phone);
		String name = target.find(phone);
		adapter.record("c=repository, s=find, r=" + name);
		return name;
	}
}