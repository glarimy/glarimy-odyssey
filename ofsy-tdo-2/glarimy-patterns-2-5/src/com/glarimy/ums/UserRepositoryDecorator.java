package com.glarimy.ums;

import java.util.Optional;

public class UserRepositoryDecorator implements UserRepository {
	private UserRepository target;

	public UserRepositoryDecorator(UserRepository target) throws Exception {
		this.target = target;
	}

	@Override
	public void add(Long phone, String name) {
		target.add(phone, name);
	}

	@Override
	public String find(Long phone) {
		return target.find(phone);
	}

	public Optional<String> findOne(Long phone) {
		String name = target.find(phone);
		if (name == null)
			return Optional.empty();
		return Optional.of(name);
	}
}