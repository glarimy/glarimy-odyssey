package com.glarimy.ums;

import java.util.HashMap;
import java.util.Map;

import com.glarimy.framework.Singleton;

@Singleton(getter = "getInstance")
public class InMemoryUserRepository implements UserRepository {
	private static InMemoryUserRepository INSTANCE = null;
	private Map<Long, String> entries;

	private InMemoryUserRepository() {
		entries = new HashMap<Long, String>();
	}

	public static InMemoryUserRepository getInstance() {
		if (INSTANCE == null)
			INSTANCE = new InMemoryUserRepository();
		return INSTANCE;
	}

	@Override
	public void add(Long phone, String name) {
		System.out.println("c=repository, s=add, a=" + phone + "&" + name);
		entries.put(phone, name);
		System.out.println("c=repository, s=add, r=void");
	}

	@Override
	public String find(Long phone) {
		System.out.println("c=repository, s=find, a=" + phone);
		String name = entries.get(phone);
		System.out.println("c=repository, s=find, r="+name);
		return name;
	}
}