package com.glarimy.framework;

import java.io.FileReader;
import java.util.Properties;

public class ObjectFactory implements Factory {
	private Properties props;

	public ObjectFactory(String source) throws Exception {
		props = new Properties();
		props.load(new FileReader(source));
	}

	@Override
	public Object get(String key) throws Exception {
		String className = props.getProperty(key);
		Class<?> claz = Class.forName(className);

		try {
			return claz.newInstance();
		} catch (IllegalAccessException | InstantiationException e) {
			Singleton singleton = claz.getAnnotation(Singleton.class);
			if (singleton != null) {
				String getter = singleton.getter();
				return claz.getMethod(getter).invoke(claz);
			}
			return claz.getMethod("getInstance").invoke(claz);
		}
	}
}