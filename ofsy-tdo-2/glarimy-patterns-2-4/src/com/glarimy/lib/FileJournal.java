package com.glarimy.lib;

import java.io.FileWriter;
import java.util.Date;

public class FileJournal {
	private FileWriter writer;

	public FileJournal() throws Exception {
		writer = new FileWriter("journal.log", true);
	}

	public void save(String message) {
		try {
			writer.write(new Date() + " " + message);
			writer.write("\n");
			writer.flush();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}