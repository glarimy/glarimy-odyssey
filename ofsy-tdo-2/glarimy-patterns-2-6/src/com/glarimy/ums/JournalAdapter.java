package com.glarimy.ums;

public interface JournalAdapter {
	public void record(String message);
}
