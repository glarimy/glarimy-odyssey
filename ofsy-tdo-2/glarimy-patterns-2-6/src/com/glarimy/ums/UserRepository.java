package com.glarimy.ums;

public interface UserRepository {
	public void add(Long phone, String name);

	public String find(Long phone);
}
