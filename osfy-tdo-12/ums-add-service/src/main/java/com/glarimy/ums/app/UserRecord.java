package com.glarimy.ums.app;

import java.util.Date;

public class UserRecord extends NewUser {
	public Date since;

	public UserRecord(String name, long phone, Date since) {
		super(name, phone);
		this.since = since;
	}

}
