import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import controller from './app/UserController.js'

const app = express();
const port = process.env.PORT || 8080;
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/user', (req, res) => {
    const phone = req.query.phone
    controller.searchByPhoneNumber(phone).then(users => {
        res.json(users);
    })
});

app.listen(port, () => console.log(`UMS Search Service running on port ${port}!`))
