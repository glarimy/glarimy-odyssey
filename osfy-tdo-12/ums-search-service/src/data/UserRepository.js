import mysql from 'mysql2/promise';
import User from '../domain/User.js';
import PhoneNumber from '../domain/PhoneNumber.js';
import Name from '../domain/Name.js';

const config = {
    db: {
        host: process.env.DB_HOST || "localhost",
        user: process.env.DB_USER || "root",
        password: process.env.DB_PASSWORD || "adminadmin",
        database: process.env.DB_DATABASE || "glarimy",
    }
};

class UserRepository {
    async findByPhoneNumber(phoneNumber) {
        const connection = await mysql.createConnection(config.db);
        const [records,] = await connection.execute(`SELECT name, phone, since from ums_users where phone=${phoneNumber}`);
        const users = new Array();
        for (let record of records)
            users.push(new User(new Name(record['name']), new PhoneNumber(record['phone']), record['since']))
        return users;
    }
}

const repo = new UserRepository();
export default repo;