class PhoneNumber {
    #value;

    constructor(val){
        let matched = String(val).match(/\d/g).length===10;
        if(!matched)
            throw "Invalid Phone Number";
        this.#value = val;
    }

    getValue(){
        return this.#value;
    }

    equals(other){
        if(this.constructor.name === other.constructor.name && this.#value === other.getValue())
            return true;
        return false;
    }
}

export default PhoneNumber;