package com.glarimy.ums.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class JournalHandler {
	@Autowired
	private JournalAdapter adapter;

	@KafkaListener(topics = "${ums.user.add.topic}", groupId = "${ums.user.add.consumer}")
	public void handle(UserRecord user) {
		adapter.record(user.toString());
	}
}