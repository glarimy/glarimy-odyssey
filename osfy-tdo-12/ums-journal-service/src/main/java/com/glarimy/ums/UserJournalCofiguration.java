package com.glarimy.ums;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.glarimy.lib.FileJournal;

@Configuration
public class UserJournalCofiguration {
	@Bean
	public FileJournal getFileJournal() throws Exception {
		return new FileJournal();
	}
}
