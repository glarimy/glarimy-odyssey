package com.glarimy.ums.app;

import java.util.Date;

public class UserRecord {
	private String name;
	private long phone;
	private Date since;

	public UserRecord() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public Date getSince() {
		return since;
	}

	public void setSince(Date since) {
		this.since = since;
	}

	public UserRecord(String name, long phone, Date since) {
		super();
		this.name = name;
		this.phone = phone;
		this.since = since;
	}

	@Override
	public String toString() {
		return "name=" + name + ", phone=" + phone + ", since=" + since;
	}

}
