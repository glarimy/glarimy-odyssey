package com.glarimy.ums.app;

import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class UserDeserializer implements Deserializer<UserRecord> {
	@Override
	public UserRecord deserialize(String topic, byte[] data) {
		try {
			return new ObjectMapper().readValue(data, UserRecord.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
