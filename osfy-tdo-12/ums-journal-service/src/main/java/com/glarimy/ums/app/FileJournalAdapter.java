package com.glarimy.ums.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.glarimy.lib.FileJournal;

@Component
public class FileJournalAdapter implements JournalAdapter {
	@Autowired
	private FileJournal journal;

	@Override
	public void record(String message) {
		journal.save(message);
	}
}