package com.glarimy.ums;

import com.glarimy.lib.FileJournal;

public class FileJournalAdapter implements JournalAdapter {
	private FileJournal journal;

	public FileJournalAdapter() throws Exception {
		journal = new FileJournal();
	}

	@Override
	public void record(String message) {
		journal.save(message);
	}
}