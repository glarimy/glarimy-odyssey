package com.glarimy.ums;

import java.util.Optional;

public interface UserRepositoryDecorator extends UserRepository {
	public Optional<String> findOne(Long phone);
}