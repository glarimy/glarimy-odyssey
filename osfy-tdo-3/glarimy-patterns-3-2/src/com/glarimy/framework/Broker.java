package com.glarimy.framework;

public interface Broker {
	public long register(Handler handler);
	public void unregister(long hid);
	public void publish(String message, String type);
}
