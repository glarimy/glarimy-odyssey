package com.glarimy.framework;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MessageBroker implements Broker {
	private Map<Long, Handler> handlers;

	public MessageBroker() {
		handlers = new HashMap<Long, Handler>();
	}

	@Override
	public void publish(String message, String type) {
		handlers.values().stream().filter(h -> h.getType().equalsIgnoreCase(type)).forEach(s -> s.handle(message));
	}

	@Override
	public long register(Handler handler) {
		long hid = new Date().getTime();
		handlers.put(hid, handler);
		return hid;
	}

	@Override
	public void unregister(long sid) {
		handlers.remove(sid);
	}
}
