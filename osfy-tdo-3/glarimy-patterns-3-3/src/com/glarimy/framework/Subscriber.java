package com.glarimy.framework;

public interface Subscriber {
	public void on(String message, String type);
}
