package com.glarimy.framework;

public interface Handler {
	public void handle(String message);

	public String getType();
}
