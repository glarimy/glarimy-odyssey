package com.glarimy.ums;

import com.glarimy.framework.Broker;
import com.glarimy.framework.Factory;
import com.glarimy.framework.ObjectFactory;

public class UserRepositoryJournalProxy implements UserRepository {
	private UserRepository target;
	private Broker broker;
	private static final String TOPIC = "com.glarimy.ums";

	public UserRepositoryJournalProxy(UserRepository target) throws Exception {
		this.target = target;
		Factory factory = new ObjectFactory("config.properties");
		this.broker = (Broker) factory.get("broker");
		this.broker.register(new JournalHandler());
	}

	@Override
	public void add(Long phone, String name) {
		broker.publish("c=repository, s=add, a=" + "&" + name, TOPIC);
		target.add(phone, name);
		broker.publish("c=repository, s=add, r=void", TOPIC);
	}

	@Override
	public String find(Long phone) {
		broker.publish("c=repository, s=find, a=" + phone, TOPIC);
		String name = target.find(phone);
		broker.publish("c=repository, s=find, r=" + name, TOPIC);
		return name;
	}
}