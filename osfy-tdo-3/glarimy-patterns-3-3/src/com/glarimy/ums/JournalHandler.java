package com.glarimy.ums;

import com.glarimy.framework.Factory;
import com.glarimy.framework.Handler;
import com.glarimy.framework.ObjectFactory;

public class JournalHandler implements Handler {
	private JournalAdapter adapter;

	public JournalHandler() throws Exception {
		Factory factory = new ObjectFactory("config.properties");
		this.adapter = (JournalAdapter) factory.get("adapter");
	}

	@Override
	public void handle(String message) {
		adapter.record(message);
	}

	@Override
	public String getType() {
		return "com.glarimy.ums";
	}
}