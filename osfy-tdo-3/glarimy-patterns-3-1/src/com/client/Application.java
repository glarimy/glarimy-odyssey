package com.client;

import com.glarimy.framework.Factory;
import com.glarimy.framework.ObjectFactory;
import com.glarimy.ums.UserRepositoryDecorator;
import com.glarimy.ums.FluentUserRepository;
import com.glarimy.ums.UserRepository;

public class Application {
	public static void main(String[] args) throws Exception {
		Factory factory = new ObjectFactory("config.properties");
		UserRepository repo = (UserRepository) factory.get("user.repository");
		repo.add(9731423166L, "Krishna");
		UserRepositoryDecorator fluent = new FluentUserRepository(repo);
		fluent.findOne(9731423166L).ifPresent(name -> System.out.println("Found " + name));
	}
}
