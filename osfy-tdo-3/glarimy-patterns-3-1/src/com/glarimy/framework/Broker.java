package com.glarimy.framework;

public interface Broker {
	public long subscribe(Subscriber subscriber);
	public void unsubscribe(long sid);
	public void publish(String message, String type);
}
