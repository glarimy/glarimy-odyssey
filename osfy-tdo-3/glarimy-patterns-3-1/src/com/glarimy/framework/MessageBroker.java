package com.glarimy.framework;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MessageBroker implements Broker {
	private Map<Long, Subscriber> subscribers;

	public MessageBroker() {
		subscribers = new HashMap<Long, Subscriber>();
	}

	@Override
	public void publish(String message, String type) {
		subscribers.values().stream().forEach(s -> s.on(message, type));
	}

	@Override
	public long subscribe(Subscriber subscriber) {
		long sid = new Date().getTime();
		subscribers.put(sid, subscriber);
		return sid;
	}

	@Override
	public void unsubscribe(long sid) {
		subscribers.remove(sid);
	}
}
